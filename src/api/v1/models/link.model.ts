import mongoose, { Document, mongo } from "mongoose";
import { customAlphabet } from "nanoid";
import {
  TLinkDocument,
  TLink as TLink,
} from "../interfaces/link-model.interface";

export const SLUG_ID_STRING_LENGTH = 10;
const nanoid = customAlphabet(
  "abcdefghijklmnopqrstuvwxyz0123456789",
  SLUG_ID_STRING_LENGTH
);

type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

export type TClickLog = ArrayElement<TLink["clicks"]>;

const ClicksSchema = new mongoose.Schema<TClickLog>({
  srcIp: { type: String, required: true },
  countryCode: { type: String, required: true },
  lat: { type: Number, required: true },
  lon: { type: Number, required: true },
  clickedAt: { type: Date, required: true },
});
export const ClicksModel = mongoose.model("Clicks", ClicksSchema);

const LinkSchema = new mongoose.Schema<TLink>({
  url: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
    unique: true,
    required: true,
    default: () => nanoid(),
  },
  clicks: [ClicksSchema],
});

LinkSchema.methods.toJSON = function () {
  const obj = this.toObject();
  delete obj._id;
  delete obj.__v;
  return obj;
};

export const ShortUrlModel = mongoose.model<TLink>("ShortUrl", LinkSchema);
