import mongoose, { Schema } from "mongoose";
import { TUser } from "../interfaces/user-model.interface";
import bcrypt from "bcrypt";

const UsersSchema = new mongoose.Schema<TUser>(
  {
    username: {
      unique: true,
      required: true,
      type: String,
    },
    email: {
      unique: true,
      type: String,
      required: false,
    },
    password: {
      required: true,
      type: String,
      select: false,
    },
    googleId: {
      required: false,
      type: String,
    },
    shortUrls: {
      type: [Schema.Types.ObjectId],
      ref: "ShortUrl",
    },
  },
  { timestamps: true }
);

UsersSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    bcrypt.hash(this.password, 8, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  }
});

export const UsersModel = mongoose.model<TUser>("Users", UsersSchema);
