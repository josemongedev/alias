import { z } from "zod";
import { SLUG_ID_STRING_LENGTH } from "../models/link.model";

const slugPattern = new RegExp(`[0-9a-zA-Z]{${SLUG_ID_STRING_LENGTH}}$`);

export const shortLinkElements = {
  url: z.string().trim().url(),
  slug: z
    .string()
    .trim()
    .refine((arg) => slugPattern.test(arg), {
      message: `Slug must be a lowercase alphanumeric value ${SLUG_ID_STRING_LENGTH} characters long`,
    }),
  clicks: z.object({
    srcIp: z.string().trim().ip(),
    countryCode: z.string().trim().length(2),
    lat: z.number(),
    lon: z.number(),
    clickedAt: z.date().optional(),
  }),
};

const ShortLinkSchema = z.object({
  url: shortLinkElements.url,
  slug: shortLinkElements.slug,
  clicks: z.array(shortLinkElements.clicks),
});

export const CreateShortLinkSchema = z.object({
  body: z.object({
    url: shortLinkElements.url,
    slug: shortLinkElements.slug.optional(),
  }),
});

export const GetShortLinkSchema = z.object({
  params: z.object({
    slug: shortLinkElements.slug,
  }),
});

export const DeleteShortLinkSchema = z.object({
  params: z.object({
    slug: shortLinkElements.slug,
  }),
});

export const UpdateShortLinkSchema = z.object({
  params: z.object({
    slug: shortLinkElements.slug,
  }),
  body: z.object({
    url: shortLinkElements.url.optional(),
    slug: shortLinkElements.slug.optional(),
    clicks: shortLinkElements.clicks.optional(),
  }),
});

export const UpsertClicksSchema = z.object({
  params: z.object({
    slug: shortLinkElements.slug,
  }),
  body: z.object({
    clicks: shortLinkElements.clicks.optional(),
  }),
});

export type TShortLinkSchema = z.infer<typeof ShortLinkSchema>;

export default ShortLinkSchema;
