import { z } from "zod";
import { shortLinkElements } from "./link.schema";

const userElements = {
  _id: z.string().trim(),
  username: z
    .string()
    .trim()
    .min(8, { message: "Must be 8 or more characters long" }),
  email: z.string().trim().email(),
  password: z
    .string()
    .trim()
    .min(8, { message: "Must be 8 or more characters long" }),
  googleId: z.string().trim(),
  // shortUrlIds: z.string().trim().array(),
};

const UserSchema = z.object({
  // _id: userElements._id,
  username: userElements.username,
  email: userElements.email.optional(),
  password: userElements.password,
  googleId: userElements.googleId,
});

export const CreateUserSchema = z.object({
  body: z.object({
    username: userElements.username,
    password: userElements.password,
    email: userElements.email,
  }),
});

export const CreateUserShortLinkByIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
  }),
  body: z.object({
    url: shortLinkElements.url,
    slug: shortLinkElements.slug.optional(),
  }),
});

export const GetUserByUsernameSchema = z.object({
  params: z.object({
    username: userElements.username,
  }),
});

export const GetUserByIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
  }),
});

export const GetLinksByUserIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
  }),
});

export const UpdateUserShortLinkByIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
    shortLinkId: userElements._id,
  }),
});

export const UpdateUserByIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
  }),
  body: z.object({
    username: userElements.username.optional(),
    email: userElements.email,
    password: userElements.password.optional(),
  }),
});

export const DeleteUserByIdSchema = z.object({
  params: z.object({
    userId: userElements._id,
  }),
});

export type TUserSchema = z.infer<typeof UserSchema>;

export default UserSchema;
