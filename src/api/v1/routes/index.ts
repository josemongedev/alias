import express from "express";
import IMessageResponse from "../interfaces/message.response";
import { authorizationHandler } from "../middleware/authorization.middleware";
import { dbStart } from "../middleware/db.middleware";
import apiRouter from "./api";
import authRouter from "./auth.route";

const aliasRouter = express.Router();

interface IHealthResponse extends IMessageResponse {
  authToken: string;
}

aliasRouter.get<{}, IHealthResponse>(
  "/health",
  authorizationHandler,
  (req, res) => {
    res.json({
      message: "Alias API server online",
      authToken: req.cookies.authorization,
    });
  }
);

aliasRouter.use(dbStart).use("/auth", authRouter).use("/api/v1", apiRouter);

aliasRouter.get("/", (req, res) => {
  res.json({ message: "Ready" });
});

export default aliasRouter;
