import express from "express";
import userController from "../../controllers/user.controller";
import validate from "../../middleware/validation.middleware";
import {
  CreateUserSchema,
  CreateUserShortLinkByIdSchema,
  DeleteUserByIdSchema,
  GetLinksByUserIdSchema,
  GetUserByIdSchema,
  GetUserByUsernameSchema,
  UpdateUserShortLinkByIdSchema as UpdateShortLinksByUserId,
  UpdateUserByIdSchema,
} from "../../schemas/user.schema";

const userRouter = express.Router();

userRouter
  .route("/:userId/:shortLinkId/")
  .put(
    validate(UpdateShortLinksByUserId),
    userController.updateShortLinksByUserId
  );

userRouter
  .route("/:userId/links/")
  .post(
    validate(CreateUserShortLinkByIdSchema),
    userController.createShortLinksByUserId
  )
  .get(validate(GetLinksByUserIdSchema), userController.getLinksByUserId);

userRouter
  .route("/:userId")
  .get(validate(GetUserByIdSchema), userController.getUserById)
  .put(validate(UpdateUserByIdSchema), userController.updateUserById)
  .delete(validate(DeleteUserByIdSchema), userController.deleteUserById);

userRouter
  .route("/username/:username")
  .get(validate(GetUserByUsernameSchema), userController.getUserByUsername);

userRouter
  .route("/")
  .get(userController.getAllUsers)
  .post(validate(CreateUserSchema), userController.createUser);

export default userRouter;
