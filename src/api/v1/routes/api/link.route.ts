import express from "express";
import linkController from "../../controllers/link.controller";
import validate from "../../middleware/validation.middleware";
import {
  CreateShortLinkSchema,
  DeleteShortLinkSchema,
  GetShortLinkSchema,
  UpdateShortLinkSchema,
  UpsertClicksSchema,
} from "../../schemas/link.schema";

const urlRouter = express.Router();

urlRouter
  .route("/:slug/clicks")
  .put(validate(UpsertClicksSchema), linkController.upsertClicksBySlug);

urlRouter
  .route("/:slug")
  .get(validate(GetShortLinkSchema), linkController.getUrlBySlug)
  .put(validate(UpdateShortLinkSchema), linkController.updateUrlBySlug)
  .delete(validate(DeleteShortLinkSchema), linkController.deleteUrlBySlug);

urlRouter
  .route("/")
  .post(validate(CreateShortLinkSchema), linkController.createNewShortUrl)
  .get(linkController.getAllUrlSlugs);

export default urlRouter;
