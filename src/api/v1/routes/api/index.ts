import express from "express";

import IMessageResponse from "../../interfaces/message.response";
import linkRouter from "./link.route";
import userRouter from "./user.route";

const apiRouter = express.Router();

apiRouter.use("/link", linkRouter);
apiRouter.use("/user", userRouter);

apiRouter.get<{}, IMessageResponse>("/", (req, res) => {
  res.json({
    message: "Alias API - v1. Database connection online",
  });
});

export default apiRouter;
