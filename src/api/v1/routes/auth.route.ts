import express from 'express';
import IMessageResponse from '../interfaces/message.response';
import { StatusCodes } from 'http-status-codes';

import { CookieOptions } from 'express';
import jwt from 'jsonwebtoken';

const cookieOptions: CookieOptions = {
  httpOnly: true, // Server side cookie
  maxAge: 24 * 60 * 60 * 1000, // time in ms set to 1 day
  // sameSite: "none", //allows to be used across different domains
  sameSite: 'none', // same domain
  secure: process.env.NODE_ENV !== 'test', // Use Https instead of Http.
  // Disabled during testing due to issues with supertest agent
};

const authRouter = express.Router();

authRouter.post<{}, IMessageResponse>('/login', (req, res) => {
  const apiKey = req.headers['access-key'] as string;
  // Just a guard in case the env var is not defined, and secrets must match
  if (!process.env.API_SECRET || apiKey !== process.env.API_SECRET) {
    return res
      .status(StatusCodes.UNAUTHORIZED)
      .json({ message: 'Unauthorized' });
  }
  // Create authorization token to access API
  const authToken = jwt.sign(
    { sessionDate: new Date().toISOString() },
    process.env.API_SECRET,
    {
      expiresIn: '1d',
    }
  );
  // Store authorization token on server cookie
  res.cookie('authorization', authToken, cookieOptions);
  return res.json({ message: 'User logged in' });
});

export default authRouter;
