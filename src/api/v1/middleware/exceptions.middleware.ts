import { ErrorRequestHandler, NextFunction, Request, Response } from "express";

import ErrorResponse from "../interfaces/error.response";
import { EResponseStatusCode } from "../interfaces/message.response";

export function notFound(req: Request, res: Response, next: NextFunction) {
  res.status(404);
  const error = new Error(`🔍 - Not Found - ${req.originalUrl}`);
  next(error);
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorHandler: ErrorRequestHandler = (
  err: Error,
  _req: Request,
  res: Response<ErrorResponse>,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _next: NextFunction
) => {
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
  res.status(statusCode);
  res.json({
    message: err.message,
    stack: process.env.NODE_ENV === "production" ? "" : err.stack,
    responseCode:
      statusCode === 500
        ? EResponseStatusCode.UNKNOWN_ERROR
        : EResponseStatusCode.ERROR,
  });
};
