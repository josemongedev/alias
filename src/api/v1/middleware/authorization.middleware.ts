import { RequestHandler } from 'express';
import { StatusCodes } from 'http-status-codes';
import jwt from 'jsonwebtoken';

export const authorizationHandler: RequestHandler = (req, res, next) => {
  res.header('Content-Type', 'application/json');
  const authToken = req?.cookies.authorization;
  if (!authToken) return res.sendStatus(StatusCodes.UNAUTHORIZED);
  try {
    const decoded = jwt.verify(authToken, process.env.API_SECRET);
    return next();
  } catch (error: any) {
    return res.status(StatusCodes.FORBIDDEN).json({});
  }
};
