import { RequestHandler } from 'express';
import { dbConnect } from '../config/db.config';

export const dbStart: RequestHandler = async (_, res, next) => {
  try {
    await dbConnect(process.env.DATABASE_URI);
    next();
  } catch (err) {
    res.json(String(err));
  }
};
