import { StatusCodes } from "http-status-codes";
import { RequestHandler } from "express";
import { AnyZodObject } from "zod";
import { ValidationError, fromZodError } from "zod-validation-error";
import IMessageResponse, {
  EResponseStatusCode,
} from "../interfaces/message.response";

export interface IValidationResponse extends IMessageResponse {
  error: string;
}

const validate =
  <T extends AnyZodObject>(
    schema: T
  ): RequestHandler<any, IValidationResponse> =>
  (req, res, next) => {
    try {
      schema.parse({
        body: req.body,
        query: req.query,
        params: req.params,
      });
      return next();
    } catch (err: any) {
      const validationError: ValidationError = fromZodError(err);
      return res.status(StatusCodes.BAD_REQUEST).json({
        message: validationError.details[0].message,
        error: validationError.details[0].code,
        responseCode: EResponseStatusCode.VALIDATION_ISSUE,
      });
    }
  };

export default validate;
