import { CookieOptions } from 'express';

const MILLISECONDS_PER_SECOND = 1000;
const SECONDS_PER_MINUTE = 60;
const MINUTES_PER_HOUR = 60;
const HOURS_PER_DAY = 24;
const ONE_DAY_AGE =
  HOURS_PER_DAY *
  MINUTES_PER_HOUR *
  SECONDS_PER_MINUTE *
  MILLISECONDS_PER_SECOND;

const cookieOptions: CookieOptions = {
  // Disabled during testing due to issues with supertest agent
  secure: process.env.NODE_ENV !== 'test', // Use Https instead of Http.
  httpOnly: true, // Server side cookie
  maxAge: ONE_DAY_AGE, // time in ms set to 1 day
  // sameSite: "none", //allows to be used across different domains
  sameSite: process.env.COOKIE_SAME_SITE_POLICY,
};

export default cookieOptions;
