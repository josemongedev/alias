import passport from "passport";
import { Strategy as GoogleStrategy } from "passport-google-oauth2";
import { UsersModel as UserModel } from "../models/user.model";

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "http://yourdomain:3000/auth/google/callback",
      passReqToCallback: true,
    },
    function (
      _request: any,
      _accessToken: any,
      _refreshToken: any,
      profile: any,
      done: any
    ) {
      UserModel.create(
        { googleId: profile.id },
        function (err: any, user: any) {
          return done(err, user);
        }
      );
    }
  )
);
