import RedisStore from 'connect-redis';
import { createClient } from 'redis';

// Initialize client.
const redisClient = createClient({
  password: process.env.REDIS_PASSWORD,
  socket: {
    host: process.env.REDIS_HOSTNAME,
    port: process.env.REDIS_PORT,
  },
});

// Alert on connection
redisClient.on('connect', () => {
  console.log('Connected to our redis instance!');
  //   client.set('Greatest Basketball Player', 'Lebron James');
});

// Connect to Redis server
redisClient.connect().catch(console.error);

// Initialize store.
export const redisStore = new RedisStore({
  client: redisClient,
  prefix: 'aliasApp:',
});
