import mongoose from 'mongoose';

export const connectOptions = {
  keepAlive: true,
  socketTimeoutMS: 2000,
  connectTimeoutMS: 2000,
};

export const dbConnect = async (URI: string) => {
  try {
    mongoose.set('strictQuery', false);
    await mongoose.connect(URI);
  } catch (err) {
    console.error(err);
  }
};
