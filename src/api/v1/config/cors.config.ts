import { CorsOptions } from 'cors';

export const corsConfig: Record<string, any> = {};

corsConfig.allowedOrigins = [];

corsConfig.setAllowedOrigins = () => {
  // Only when env variables have been set and after being loaded
  if (process.env.ALLOWED_ORIGINS)
    corsConfig.allowedOrigins = process.env.ALLOWED_ORIGINS.split(',');
};

corsConfig.options = {
  origin: (origin, callback) => {
    // allow requests with no origin
    // (like mobile apps or curl requests)
    if (!origin) return callback(null, true);
    if (corsConfig.allowedOrigins.indexOf(origin) === -1) {
      var msg =
        'The CORS policy for this site does not ' +
        'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
  optionsSuccessStatus: 200,
  credentials: true,
} as CorsOptions;
