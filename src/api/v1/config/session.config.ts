import { SessionOptions } from 'express-session';
import { redisStore } from './redis.config';
import cookieOptions from './cookie.config';
import { v4 as uuid } from 'uuid';

export const sessionConfig: SessionOptions = {
  genid: (req) => {
    console.log('Inside the session middleware');
    console.log(req.sessionID);
    return uuid(); // use UUIDs for session IDs
  },
  name: 'sid-alias',
  // store: redisStore,
  resave: false, // required: force lightweight session keep alive (touch)
  saveUninitialized: false, // recommended: only save session when data exists
  // cookie: cookieOptions,
  secret: process.env.SESSION_SECRET,
};
