import { TLink } from "../interfaces/link-model.interface";
import { TPostNewShortLink as TCreateNewShortLink } from "../interfaces/link.request";
import { ClicksModel, ShortUrlModel, TClickLog } from "../models/link.model";

class ShortUrlDocument {
  create = async (newLink: TCreateNewShortLink) =>
    await ShortUrlModel.create(newLink);

  getAllUrlSlugs = async () => {
    const data = await ShortUrlModel.find().exec();
    return data;
  };

  getUrlById = async (_id: string) => {
    const data = await ShortUrlModel.findOne({ _id }).exec();
    return data;
  };

  getUrlBySlug = async (slug: string) => {
    const data = await ShortUrlModel.findOne({ slug }).exec();
    return data;
  };

  updateUrlBySlug = async (slug: string, updates: Partial<TLink>) => {
    const shortLink = await ShortUrlModel.findOne({ slug }, updates).exec();
    if (!shortLink) return { result: null, data: null };
    const result = await ShortUrlModel.updateOne({ slug }, updates, {
      new: true,
    }).exec();
    return {
      result,
      data: shortLink.toObject(),
    };
  };

  updateClicksBySlug = async (slug: string, clickData: TClickLog) => {
    const shortLink = await ShortUrlModel.findOne({ slug }).exec();
    if (!shortLink) return { result: null, data: null };
    clickData.clickedAt = new Date();
    shortLink.clicks.push(new ClicksModel(clickData));
    const result = await ShortUrlModel.updateOne(
      { slug },
      { clicks: shortLink.clicks },
      { new: true }
    ).exec();
    return { result, data: shortLink.clicks.at(-1) };
  };

  deleteUrlBySlug = async (slug: string) => {
    const result = await ShortUrlModel.findOneAndDelete({ slug }).exec();
    return result;
  };
}

const urlDocument = new ShortUrlDocument();
export default urlDocument;
