import { TUser } from "../interfaces/user-model.interface";
import { TNewUserBody } from "../interfaces/user.request";
import { UsersModel } from "../models/user.model";

class UserDocument {
  createUser = async (newUser: TNewUserBody) =>
    await UsersModel.create(newUser);

  getAlUsers = async () => {
    const data = await UsersModel.find().exec();
    return data;
  };

  getUserByUsername = async (username: string) => {
    const data = await UsersModel.findOne({ username })
      .select("+password")
      .exec();
    return data;
  };

  getLinksByUserId = async (_id: string) => {
    const data = await UsersModel.findOne({ _id })
      .select("shortUrls")
      .populate("shortUrls")
      .exec();
    return data ? data.shortUrls : null;
  };

  getUserById = async (_id: string) => {
    const data = await UsersModel.findOne({ _id }).exec();
    return data;
  };

  updateUserById = async (_id: string, updates: Partial<TUser>) => {
    const user = await UsersModel.findOne({ _id }, updates).exec();
    if (!user) return { result: null, data: null };
    const result = await UsersModel.updateOne({ _id }, updates, {
      new: true,
    }).exec();
    return {
      result,
      data: user,
    };
  };

  deleteUserById = async (_id: string) => {
    const result = await UsersModel.findOneAndDelete({ _id }).exec();
    return result;
  };
}

const userDocument = new UserDocument();
export default userDocument;
