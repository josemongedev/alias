import { TShortLinkSchema as TLinkSchema } from "../schemas/link.schema";

export type TLink = TLinkSchema;

export type TLinkDocument = TLink & Document;
