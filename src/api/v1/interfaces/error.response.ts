import IMessageResponse from './message.response';

export default interface ErrorResponse extends IMessageResponse {
  stack?: string;
}
