import { RequestHandler } from "express";
import { TUserSchema } from "../schemas/user.schema";
import IUserResponse, {
  IUserDeleteResponse,
  IUserLinksResponse,
} from "./user.response";
import { TUserDocument } from "./user-model.interface";
import IShortUrlResponse from "./link.response";

export type TNewUserBody = Pick<TUserSchema, "username" | "password">;

export type TCreateNewUserRequest = RequestHandler<
  {},
  IUserResponse,
  TNewUserBody
>;

export type TGetAllUsersRequest = RequestHandler<{}, IUserResponse>;

export type TGetUserByUsernameRequest = RequestHandler<
  Pick<TUserSchema, "username">,
  IUserResponse
>;
export type TGetUserByIdRequest = RequestHandler<
  { userId: string },
  IUserResponse
>;
export type TGetLinksByUserIdRequest = RequestHandler<
  { userId: string },
  IUserLinksResponse<TUserDocument["shortUrls"]>
>;

export type TDeleteUserByIdRequest = RequestHandler<
  { userId: string },
  IUserDeleteResponse
>;
export type TUpdateUserByIdRequest = RequestHandler<
  { userId: string },
  IUserResponse
>;

export type TCreateShortLinksByUseIdRequest = RequestHandler<
  { userId: string },
  IShortUrlResponse
>;

export type TUpdateShortLinksByUseIdRequest = RequestHandler<
  { userId: string; shortLinkId: string },
  IUserResponse
>;
