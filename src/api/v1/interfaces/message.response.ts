export enum EResponseStatusCode {
  ERROR = 0,
  UNKNOWN_ERROR = 1,
  VALIDATION_ISSUE = 2,
  NOT_FOUND = 3,
  SUCCESS = 4,
  CREATE_SUCCESS = 5,
  UPDATE_SUCCESS = 6,
  DELETE_SUCCESS = 7,
}

export default interface IMessageResponse {
  message: string;
  responseCode?: EResponseStatusCode;
}
