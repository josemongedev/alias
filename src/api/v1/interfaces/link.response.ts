import IMessageResponse from "./message.response";
import { TLinkDocument } from "./link-model.interface";

export default interface IShortUrlResponse extends IMessageResponse {
  data?: Partial<TLinkDocument> | Partial<TLinkDocument>[];
}
export interface IClickUpsertResponse extends IMessageResponse {
  data?: any;
}
