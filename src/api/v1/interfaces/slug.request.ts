import { RequestHandler } from "express";
import { TShortLinkSchema } from "../schemas/link.schema";

export type TGetRedirectToUrlHandler = RequestHandler<
  Pick<TShortLinkSchema, "slug">
>;
