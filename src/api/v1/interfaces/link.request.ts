import { RequestHandler } from "express";
import { TShortLinkSchema } from "../schemas/link.schema";
import IShortUrlResponse, { IClickUpsertResponse } from "./link.response";
import { z } from "zod";
import { UpsertClicksSchema } from "../schemas/link.schema";

export type TPostNewShortLink = Pick<TShortLinkSchema, "url"> &
  Partial<Pick<TShortLinkSchema, "slug">>;

export type TCreateNewShortLinkRequest = RequestHandler<
  {},
  IShortUrlResponse,
  TPostNewShortLink
>;

export type TGetAllShortLinksRequest = RequestHandler<{}, IShortUrlResponse>;

export type TGetShortLinkBySlugRequest = RequestHandler<
  Pick<TShortLinkSchema, "slug">,
  IShortUrlResponse
>;

export type TUpdateShortLinkBySlugRequest = RequestHandler<
  Pick<TShortLinkSchema, "slug">,
  IShortUrlResponse
>;
export type TUpsertClicksBySlugRequest = RequestHandler<
  Pick<TShortLinkSchema, "slug">,
  IClickUpsertResponse,
  Required<z.infer<typeof UpsertClicksSchema>["body"]>
>;

export type TDeleteShortLinkBySlugRequest = RequestHandler<
  Pick<TShortLinkSchema, "slug">,
  IShortUrlResponse
>;
