import IMessageResponse from "./message.response";
import { TUserDocument } from "./user-model.interface";

export default interface IUserResponse extends IMessageResponse {
  data?: Partial<TUserDocument> | Partial<TUserDocument>[];
}

export interface IUserLinksResponse<T> extends IMessageResponse {
  data?: T;
}

export interface IUserDeleteResponse extends IMessageResponse {
  data?: object;
}
