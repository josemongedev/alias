import { Schema } from "mongoose";
import { TUserSchema } from "../schemas/user.schema";

export type TUser = TUserSchema & { shortUrls: [Schema.Types.ObjectId] };

export type TUserDocument = TUser & Document;
