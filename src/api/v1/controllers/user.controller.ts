import { StatusCodes } from "http-status-codes";
import { EResponseStatusCode } from "../interfaces/message.response";
import {
  TCreateNewUserRequest,
  TCreateShortLinksByUseIdRequest,
  TDeleteUserByIdRequest,
  TGetAllUsersRequest,
  TGetLinksByUserIdRequest,
  TGetUserByIdRequest,
  TGetUserByUsernameRequest,
  TUpdateShortLinksByUseIdRequest as TUpdateShortLinksByUserIdRequest,
  TUpdateUserByIdRequest,
} from "../interfaces/user.request";
import urlDocument from "../services/link.document";
import userDocument from "../services/user.document";

class UserController {
  createUser: TCreateNewUserRequest = async (req, res, next) => {
    const user = req.body;
    const data = await userDocument.createUser(user);
    if (!data) {
      res.status(StatusCodes.CONFLICT);
      const error = new Error("Duplicate username found, already in use");
      next(error);
    }
    return res.json({
      data,
      message: "User created successfully!",
      responseCode: EResponseStatusCode.CREATE_SUCCESS,
    });
  };

  createShortLinksByUserId: TCreateShortLinksByUseIdRequest = async (
    req,
    res,
    next
  ) => {
    // Check user exists
    const userId = req.params.userId;

    const user = await userDocument.getUserById(userId);
    if (!user) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error(`User with ID ${userId} not found`);
      return next(error);
    }
    // Optionally check slug doesn't exists
    const slug = req.body.slug;
    if (slug) {
      const result = await urlDocument.getUrlBySlug(slug);
      if (result) {
        res.status(StatusCodes.CONFLICT);
        const error = new Error("Duplicate slug found, already in use");
        return next(error);
      }
    }
    const newUrl = await urlDocument.create(req.body);
    if (!newUrl) {
      const error = new Error("Couldn't create the short link");
      return next(error);
    }
    user.shortUrls.push(newUrl._id);
    const shortUrls = user.shortUrls;
    await userDocument.updateUserById(userId, { shortUrls });
    return res.json({
      data: newUrl,
      message: `Short link created at /${newUrl.slug}`,
      responseCode: EResponseStatusCode.CREATE_SUCCESS,
    });
  };

  updateShortLinksByUserId: TUpdateShortLinksByUserIdRequest = async (
    req,
    res,
    next
  ) => {
    const userId = req.params.userId;
    const shortLinkId = req.params.shortLinkId;
    const shortLink = await urlDocument.getUrlById(shortLinkId);
    const user = await userDocument.getUserById(userId);
    if (!user || !shortLink) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error(`User with ID ${userId} not found`);
      return next(error);
    }
    user.shortUrls.push(shortLink._id);
    const updates = await user.save();
    // const updated=await userDocument.updateUserById(userId,{shortUrlIds:})

    return res.json({
      data: updates,
      message: `User with ID ${userId} successfully found`,
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  getAllUsers: TGetAllUsersRequest = async (req, res, next) => {
    const users = await userDocument.getAlUsers();
    return res.json({
      data: users,
      message: "Users fetched",
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  getLinksByUserId: TGetLinksByUserIdRequest = async (req, res, next) => {
    const userId = req.params.userId;
    const shortUrls = await userDocument.getLinksByUserId(userId);
    if (!shortUrls) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error(`User with ID ${userId} not found`);
      return next(error);
    }
    return res.json({
      data: shortUrls,
      message: `User with ID ${userId} successfully found`,
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  getUserById: TGetUserByIdRequest = async (req, res, next) => {
    const userId = req.params.userId;
    const user = await userDocument.getUserById(userId);
    if (!user) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error(`User with ID ${userId} not found`);
      return next(error);
    }
    return res.json({
      data: user,
      message: `User with ID ${userId} successfully found`,
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  getUserByUsername: TGetUserByUsernameRequest = async (req, res, next) => {
    const username = req.params.username;
    const user = await userDocument.getUserByUsername(username);
    if (!user) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error(`User with username ${username} not found`);
      return next(error);
    }
    return res.json({
      data: user,
      message: "Username found",
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  updateUserById: TUpdateUserByIdRequest = async (req, res, next) => {
    const userId = req.params.userId;
    const updates = req.body;
    const { result, data } = await userDocument.updateUserById(userId, updates);
    if (!data || !result?.acknowledged) {
      const error = new Error(
        "An unknown error happened during user deletion attempt"
      );
      return next(error);
    }
    return res.json({
      data,
      message: "User successfully updated",
      responseCode: EResponseStatusCode.UPDATE_SUCCESS,
    });
  };
  deleteUserById: TDeleteUserByIdRequest = async (req, res, next) => {
    const userId = req.params.userId;
    const data = await userDocument.deleteUserById(userId);
    if (!data) {
      const error = new Error(
        "An unknown error happened during user deletion attempt"
      );
      return next(error);
    }
    return res.json({
      data,
      message: "User successfully deleted",
      responseCode: EResponseStatusCode.DELETE_SUCCESS,
    });
  };
}
const userController = new UserController();

export default userController;
