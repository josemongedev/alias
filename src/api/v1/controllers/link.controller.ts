import { StatusCodes } from "http-status-codes";
import { EResponseStatusCode } from "../interfaces/message.response";
import {
  TCreateNewShortLinkRequest,
  TDeleteShortLinkBySlugRequest,
  TGetAllShortLinksRequest,
  TGetShortLinkBySlugRequest,
  TUpdateShortLinkBySlugRequest,
  TUpsertClicksBySlugRequest,
} from "../interfaces/link.request";
import urlDocument from "../services/link.document";

class LinkController {
  createNewShortUrl: TCreateNewShortLinkRequest = async (req, res, next) => {
    if (req.body.slug) {
      const result = await urlDocument.getUrlBySlug(req.body.slug);
      if (result) {
        res.status(StatusCodes.CONFLICT);
        const error = new Error("Duplicate slug found, already in use");
        next(error);
      }
    }
    const data = await urlDocument.create(req.body);
    if (!data) {
      const error = new Error("Couldn't create the short link");
      next(error);
    }
    return res.json({
      data,
      message: `Short link created at /${data.slug}`,
      responseCode: EResponseStatusCode.CREATE_SUCCESS,
    });
  };

  getAllUrlSlugs: TGetAllShortLinksRequest = async (req, res) => {
    const data = await urlDocument.getAllUrlSlugs();
    return res.json({
      data,
      message:
        data.length === 0
          ? "No short links have been created"
          : "Short links found",
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  getUrlBySlug: TGetShortLinkBySlugRequest = async (req, res, next) => {
    const { slug } = req.params;
    const data = await urlDocument.getUrlBySlug(slug);
    if (!data) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error("Requested slug doesn't exist");
      return next(error);
    }
    return res.json({
      data,
      message: `Slug ${data.slug} found successfully`,
      responseCode: EResponseStatusCode.SUCCESS,
    });
  };

  upsertClicksBySlug: TUpsertClicksBySlugRequest = async (req, res, next) => {
    const { slug } = req.params;
    const clicks = req.body.clicks;

    const { data, result } = await urlDocument.updateClicksBySlug(slug, clicks);
    if (!result || !data) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error("Slug doesn't exist, update failed");
      return next(error);
    }
    if (!result.acknowledged) {
      const error = new Error("Update to database failed due to unknown error");
      return next(error);
    }
    return res.json({
      data,
      message: `Slug clicks updated successfully to /${slug}`,
      responseCode: EResponseStatusCode.DELETE_SUCCESS,
    });
  };

  updateUrlBySlug: TUpdateShortLinkBySlugRequest = async (req, res, next) => {
    const { slug } = req.params;
    const updates = req.body;
    const { data, result } = await urlDocument.updateUrlBySlug(slug, updates);
    if (!result || !data) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error("Slug doesn't exist, update failed");
      return next(error);
    }
    if (!result.acknowledged) {
      const error = new Error("Update to database failed due to unknown error");
      return next(error);
    }
    return res.json({
      data,
      message: `Slug updated successfully to ${data.slug}`,
      responseCode: EResponseStatusCode.DELETE_SUCCESS,
    });
  };

  deleteUrlBySlug: TDeleteShortLinkBySlugRequest = async (req, res, next) => {
    const { slug } = req.params;
    const data = await urlDocument.deleteUrlBySlug(slug);
    if (!data) {
      res.status(StatusCodes.NOT_FOUND);
      const error = new Error("Delete failed. Check whether the slug exists");
      return next(error);
    }
    return res.json({
      data,
      message: "Slug deleted successfully",
      responseCode: EResponseStatusCode.DELETE_SUCCESS,
    });
  };
}

const linkController = new LinkController();

export default linkController;
