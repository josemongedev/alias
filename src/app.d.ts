export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      [k: string]: string;
      readonly API_SECRET: string;
      readonly ACCESS_TOKEN_SECRET: string;
      readonly REFRESH_TOKEN_SECRET: string;
      readonly USER_INFO_TOKEN_SECRET: string;
      readonly NODE_ENV: string;
      readonly COOKIE_SAME_SITE_POLICY: "strict" | "lax" | "none";
      readonly PORT: string;
      readonly SECURE_COOKIE: string;
      readonly DATABASE_URI: string;
      readonly ALLOWED_ORIGINS: string;
      readonly API_V1_PATH: string;
      readonly REDIS_HOSTNAME: string;
      readonly REDIS_PORT: number;
      readonly REDIS_PASSWORD: string;
      readonly SESSION_SECRET: string;
      readonly GOOGLE_CLIENT_ID: string;
      readonly GOOGLE_CLIENT_SECRET: string;
      readonly GOOGLE_OAUTH_REDIRECT_URL: string;
    }
  }
  namespace Express {
    interface Request {}
    interface Response {}
    interface Locals {}
    interface Application {}
  }
}
