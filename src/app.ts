import dotenv from 'dotenv';
dotenv.config();

import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import session from 'express-session';
import { sessionConfig } from './api/v1/config/session.config';
import * as exceptions from './api/v1/middleware/exceptions.middleware';
import aliasRouter from './api/v1/routes';
import cookieParser from 'cookie-parser';

const app = express();

app.use(helmet());
app.use(morgan('tiny'));
app.use(cors());
// app.use(cors(corsConfig));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(session(sessionConfig));
app.use(cookieParser());

// app.use(passport.initialize());
// app.use(passport.session());

app.use(aliasRouter);

app.use(exceptions.notFound);
app.use(exceptions.errorHandler);

export default app;
